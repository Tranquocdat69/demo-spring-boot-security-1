package com.example.springBootLoginRegister;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class AppController {

    @Autowired
    private UserRepository repo;

    @GetMapping("")
    public String viewHomePage(){
        return "index";
    }

    @GetMapping("login")
    public String showLoginForm(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || authentication instanceof AnonymousAuthenticationToken){
            return "login";
        }
        return "redirect:/";
    }

    @GetMapping("register")
    public String showSignupForm(Model model){
        model.addAttribute("user",new User());
        return "signup_form";
    }

    @PostMapping("process_register")
    public String register(User user){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String rawPassword = user.getPassword();
        String encodedPassword = encoder.encode(rawPassword);
        user.setPassword(encodedPassword);
        repo.save(user);
        return "register_success";
    }

    @GetMapping("list_users")
    public String viewAllUsers(Model model){
        List<User> allUsers = repo.findAll();
        model.addAttribute("listUsers",allUsers);
        return "users";
    }

    @GetMapping("list_users2")
    public String viewAllUsers2(Model model){
        List<User> allUsers = repo.findAll();
        model.addAttribute("listUsers",allUsers);
        return "users";
    }

}
