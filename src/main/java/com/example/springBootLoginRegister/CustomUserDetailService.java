package com.example.springBootLoginRegister;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository repo;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User userByEmail = repo.findByEmail(email);
        if (userByEmail == null){
            throw  new UsernameNotFoundException("User not found");
        }
        return new CustomUserDetails(userByEmail);
    }
}
